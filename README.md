# webdav-server
Simple webdav server.

# WARNING
No passwork protection. Intended to be used in closed networks!

# Usage
```yaml
version: "2"

services:
  webdav:
    image: registry.gitlab.com/yazasnyal-docker/webdav-server:latest
    volumes:
      - ./data:/data
    ports:
      - 8800:80
```
