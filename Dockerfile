FROM ubuntu:20.04

RUN apt-get update && apt-get install -y nginx-full && rm /etc/nginx/sites-enabled/*
RUN mkdir /data

COPY nginx.conf /etc/nginx/nginx.conf
ENTRYPOINT [ "nginx", "-g", "daemon off;"]
